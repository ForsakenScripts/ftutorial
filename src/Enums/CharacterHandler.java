package Enums;

import Utils.InterfaceUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;

public enum CharacterHandler {
    GENDER(137),
    HEAD(113),
    JAW(114),
    TORSO(115),
    ARMS(116),
    HANDS(117),
    LEGS(118),
    FEET(119),
    HAIR(121),
    TORSOCOLOR(127),
    SKIN(131);

    private static InterfaceUtil interfaceUtil = new InterfaceUtil();
    private int childInterface;

    CharacterHandler(int childInterface) {
        this.childInterface = childInterface;
    }

    public static void createCharacter() {
        for(CharacterHandler c: CharacterHandler.values()) {
            int characterClicks = Random.high(1, 5);
            if(interfaceUtil.isCharacterOpen()) {
                for(int click = 0; click < characterClicks; click++) {
                    Interfaces.getComponent(269, c.childInterface).click();
                    Time.sleep(288, 321);
                }
            }
        }
        if(interfaceUtil.canAccept()) interfaceUtil.clickAccept();
    }

}
