package Enums;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.query.NpcQueryBuilder;
import org.rspeer.runetek.api.scene.Npcs;

public enum NpcHandler {
    GUIDE(3308, 9),
    SURVIVAL_GUIDE(8503, 8),
    FISHING_SPOT(3317, 12),
    COOK_GUIDE(3305, 6),
    QUEST_GUIDE(3312, 8),
    MINING_GUIDE(3311, 7),
    COMBAT_GUIDE(3307, 7),
    BANK_GUIDE(3310, 8),
    BROTHER_BRACE(3319, 8),
    RAT(3313, 7),
    ACCOUNT_GUIDE(3310, 8),
    CHICKEN(3316, 12),
    WIZARD(3309, 8);



    private int npcID, npcDistance;

    NpcHandler(int npcID, int npcDistance) {
        this.npcID = npcID;
        this.npcDistance = npcDistance;
    }

    public static Npc getNpc(NpcHandler npc) {
        NpcQueryBuilder query = Npcs.newQuery().ids(npc.npcID).within(npc.npcDistance);
        return Npcs.getNearest(query);
    }

}
