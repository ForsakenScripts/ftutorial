package Enums;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.query.SceneObjectQueryBuilder;
import org.rspeer.runetek.api.scene.SceneObjects;

public enum ObjectHandler {
    DOOR_IN(14, 9721, 9709, 9398, 9723),
    DOOR_OUT(14, 9722, 9710),
    DOOR_MIDDLE(10, 9716),
    GATE_IN(16,  9718, 9470),
    GATE_OUT(12, 9720),
    LADDER(16, 9727, 9726),
    TREE(12, 9730),
    FIRE(12, 26185),
    FURNACE(12, 9736, 10082),
    TIN_ORE(12, 10080),
    COPPER_ORE(12, 10079),
    ANVIL(12, 2097),
    BANK(8, 10083),
    POLL(8, 26815);



    private int objectDistance;
    private int[] objectID;
    ObjectHandler(int objectDistance, int... objectID){
        this.objectDistance =  objectDistance;
        this.objectID = objectID;
    }

    public static SceneObject getObject(ObjectHandler object){
        SceneObjectQueryBuilder query = SceneObjects.newQuery().ids(object.objectID).within(object.objectDistance);
        return SceneObjects.getNearest(query);
    }
}
