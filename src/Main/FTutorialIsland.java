package Main;

import Tasks.*;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

@ScriptMeta(developer = "WaRp", desc = "Does Tutorial Island", version = 1.0 ,name = "FTutIsland", category = ScriptCategory.OTHER)
public class FTutorialIsland extends TaskScript {

    private static Task[] Tasks = {new InterfaceTask(), new CharacterTask(), new DialogTask(), new GuideTask(), new SurvivalTask(), new CooksTask(), new QuestTask(), new MiningTask(),  new CombatTask(), new BankTask(), new PriestTask(), new WizardTask()};
    @Override
    public void onStart() {
    submit(Tasks);
    }
}
