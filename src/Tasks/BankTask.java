package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.script.task.Task;

public class BankTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 14 && VarpUtil.getProgress() <= 15;
    }

    @Override
    public int execute() {
        switch(VarpUtil.getSectionProgress()) {
            case 510:
                if(ObjectHandler.getObject(ObjectHandler.BANK) == null) {
                    WalkUtil.walkTo(Area.rectangular(3120, 3123, 3124, 3120));
                } else if(ObjectHandler.getObject(ObjectHandler.BANK) != null && !Bank.isOpen()){
                    ObjectHandler.getObject(ObjectHandler.BANK).interact(x -> true);
                    Time.sleepUntil(() -> Bank.isOpen(), 3642);
                }
                break;
            case 520:
                if(Bank.isOpen()) Bank.close();
                if(ObjectHandler.getObject(ObjectHandler.POLL) != null && !Interfaces.isOpen(310)) {
                    ObjectHandler.getObject(ObjectHandler.POLL).interact(x -> true);
                    Time.sleep(688, 1232);
                }
                break;
            case 525:
                Interfaces.closeAll();
                Time.sleepUntil(() -> !Interfaces.isOpen(310), 3273);
                if(ObjectHandler.getObject(ObjectHandler.DOOR_IN) != null) ObjectHandler.getObject(ObjectHandler.DOOR_IN).interact(x -> true);
                break;
            case 530:
                if(NpcHandler.getNpc(NpcHandler.ACCOUNT_GUIDE) != null) NpcHandler.getNpc(NpcHandler.ACCOUNT_GUIDE).interact(x -> true);
                break;
            case 531:
               if(!Tabs.isOpen(Tab.ACCOUNT_MANAGEMENT)) Tabs.open(Tab.ACCOUNT_MANAGEMENT);
                break;
            case 532:
                if(NpcHandler.getNpc(NpcHandler.ACCOUNT_GUIDE) != null) NpcHandler.getNpc(NpcHandler.ACCOUNT_GUIDE).interact(x -> true);
                break;
            case 540:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_OUT) != null) ObjectHandler.getObject(ObjectHandler.DOOR_OUT).interact(x -> true);
                break;
        }
        return Random.high(362, 488);
    }
}
