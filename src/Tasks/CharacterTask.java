package Tasks;

import Enums.CharacterHandler;
import Utils.InterfaceUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.script.task.Task;

public class CharacterTask extends Task {

    public InterfaceUtil interfaceUtil = new InterfaceUtil();

    @Override
    public boolean validate() {
        return interfaceUtil.isCharacterOpen();
    }

    @Override
    public int execute() {
        CharacterHandler.createCharacter();
        Time.sleepUntil(() -> !interfaceUtil.isCharacterOpen(), 23232);
        return Random.high(362, 488);
    }
}
