package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class CombatTask extends Task {

    Area ratArea = Area.rectangular(3097, 9514, 3110, 9520);

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 10 && VarpUtil.getProgress() <= 12;
    }

    @Override
    public int execute() {
        switch (VarpUtil.getSectionProgress()) {
            case 370:
                if (NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3105, 9507, 3108, 9510));
                } else if (NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) != null) {
                    NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE).interact(x -> true);
                }
                break;
            case 390:
                if (!Tab.EQUIPMENT.isOpen()) Tabs.open(Tab.EQUIPMENT);
                Time.sleepUntil(() -> Tab.EQUIPMENT.isOpen(), 2343);
                break;
            case 400:
                if (!Interfaces.isOpen(84)) {
                    Interfaces.getComponent(387, 1).click();
                    Time.sleepUntil(() -> Interfaces.isOpen(84), 3625);
                }
                break;
            case 405:
                if (Interfaces.isOpen(84)) {
                    Inventory.getFirst("Bronze dagger").interact(x -> true);
                    Time.sleepUntil(() -> EquipmentSlot.MAINHAND.getItemName().equals("Bronze dagger"), 2531);
                }
                break;
            case 410:
                if (NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3105, 9507, 3108, 9510));
                } else if (NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) != null) {
                    NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE).interact(x -> true);
                }
                break;
            case 420:
                if(!EquipmentSlot.MAINHAND.getItemName().equals("Bronze sword")) {
                    Inventory.getFirst("Bronze sword").interact(x -> true);
                    Time.sleepUntil(() -> EquipmentSlot.MAINHAND.getItemName().equals("Bronze sword"), 3622);
                }
                if (!EquipmentSlot.OFFHAND.getItemName().equals("Wooden shield")) {
                    Inventory.getFirst("Wooden shield").interact(x -> true);
                    Time.sleepUntil(() -> EquipmentSlot.OFFHAND.getItemName().equals("Wooden shield"), 3622);
                }
                break;
            case 430:
                if (!Tab.COMBAT.isOpen()) Tabs.open(Tab.COMBAT);
                Time.sleepUntil(() -> Tab.COMBAT.isOpen(), 2343);
                break;
            case 440:
                if(ObjectHandler.getObject(ObjectHandler.GATE_OUT) != null) {
                    ObjectHandler.getObject(ObjectHandler.GATE_OUT).interact(x -> true);
                } else {
                    WalkUtil.walkTo(ratArea);
                }
                break;
            case 450:
            case 460:
                if(NpcHandler.getNpc(NpcHandler.RAT) != null && Players.getLocal().getTargetIndex() == -1) {
                    NpcHandler.getNpc(NpcHandler.RAT).interact(x -> true);
                    Time.sleepUntil(() -> Players.getLocal().getTargetIndex() != -1, 2364);
                }
                break;
            case 470:
                if(ObjectHandler.getObject(ObjectHandler.GATE_OUT) != null && ratArea.contains(Players.getLocal().getPosition())){
                    ObjectHandler.getObject(ObjectHandler.GATE_OUT).interact(x -> true);
                    Time.sleepUntil(() -> !Players.getLocal().isAnimating(), 3212);
                } else if(NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) == null) {
                WalkUtil.walkTo(Area.rectangular(3105, 9507, 3108, 9510));
            } else if (NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE) != null) {
                    NpcHandler.getNpc(NpcHandler.COMBAT_GUIDE).interact(x -> true);
            }
                break;
            case 480:
            case 490:
                if(Inventory.contains("Shortbow")) {
                    Inventory.getFirst("Shortbow").interact(x -> true);
                    Time.sleepUntil(() -> EquipmentSlot.MAINHAND.getItemName().equals("Shortbow"), 2734);
                }
                if(Inventory.contains("Bronze arrow")) {
                    Inventory.getFirst("Bronze arrow").interact(x -> true);
                    Time.sleepUntil(() -> EquipmentSlot.QUIVER.getItemName().equals("Bronze arrow"), 2734);
                }
                if(EquipmentSlot.MAINHAND.getItemName().equals("Shortbow") && EquipmentSlot.QUIVER.getItemName().equals("Bronze arrow")) {
                    NpcHandler.getNpc(NpcHandler.RAT).interact(x -> true);
                    Time.sleepUntil(() -> Players.getLocal().getTargetIndex() != -1,  3822);
                }
                break;
            case 500:
                if(ObjectHandler.getObject(ObjectHandler.LADDER) ==  null) {
                    WalkUtil.walkTo(Area.rectangular(3109, 9526, 3112, 9523));
                } else if(ObjectHandler.getObject(ObjectHandler.LADDER) != null) {
                    ObjectHandler.getObject(ObjectHandler.LADDER).interact(x -> true);
                }
                break;
        }
            return Random.high(431, 565);

    }
}
