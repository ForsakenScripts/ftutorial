package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.script.task.Task;

public class CooksTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 4 && VarpUtil.getProgress() <= 5;
    }

    @Override
    public int execute() {
        switch(VarpUtil.getSectionProgress()){
            case 130:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_IN) == null) {
                    WalkUtil.walkTo(Area.rectangular(3079, 3085, 3081, 3081));
                } else if(ObjectHandler.getObject(ObjectHandler.DOOR_IN) != null) ObjectHandler.getObject(ObjectHandler.DOOR_IN).interact(x -> true);
                break;
            case 140:
                if(NpcHandler.getNpc(NpcHandler.COOK_GUIDE) != null) NpcHandler.getNpc(NpcHandler.COOK_GUIDE).interact(x -> true);
                break;
            case 150:
                if(Inventory.contains("Pot of flour") && Inventory.contains("Bucket of water")) Inventory.use(item -> item.getName().equals("Pot of flour"), Inventory.getFirst("Bucket of water"));
                Time.sleepUntil(() -> Inventory.contains("Bread dough"), 3723);
                break;
            case 160:
                if(Inventory.contains("Bread dough")) {
                    Inventory.use(item -> item.getName().equals("Bread dough"), ObjectHandler.getObject(ObjectHandler.FURNACE));
                    Time.sleepUntil(() -> Inventory.getSelectedItem().equals("Bread dough"), 234);
                    Inventory.use(item -> item.getName().equals("Bread dough"), ObjectHandler.getObject(ObjectHandler.FURNACE));
                    Time.sleepUntil(() -> Inventory.contains("Bread"), 1231);
                }
                break;
            case 170:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_OUT) != null) ObjectHandler.getObject(ObjectHandler.DOOR_OUT).interact(x -> true);
                break;
        }





        return Random.high(221, 324);
    }
}
