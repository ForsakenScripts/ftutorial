package Tasks;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.script.task.Task;

public class DialogTask extends Task {

    public String[] chatOptions = {"I am an experienced player", "Yes.", "No, I'm not planning to do that.", "I'm fine, thanks."};

    @Override
    public boolean validate() {
        return Dialog.canContinue() || Dialog.isOpen() || Interfaces.getComponent(162, 45).isVisible();
    }

    @Override
    public int execute() {
        if(Dialog.isViewingChatOptions()) {
            Dialog.process(chatOptions);
        }
        if(Dialog.canContinue()) {
            Dialog.processContinue();
        }
        if(Interfaces.getComponent(162, 45).isVisible() && Interfaces.getComponent(162, 45).getText().equals("Click to continue")) {
            Game.getClient().fireScriptEvent(299, 1, 1);
            Interfaces.getComponent(162, 45).interact(0);
        }

        return Random.high(122, 465);
    }
}
