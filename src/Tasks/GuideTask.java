package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.script.task.Task;

public class GuideTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() <= 1;
    }

    @Override
    public int execute() {
        switch(VarpUtil.getSectionProgress()){
            case 0:
            case 1:
            case 2:
                if(NpcHandler.getNpc(NpcHandler.GUIDE) != null) NpcHandler.getNpc(NpcHandler.GUIDE).interact(x -> true);
                break;
            case 3:
                if(!Tab.OPTIONS.isOpen()) Tabs.open(Tab.OPTIONS);
                break;
            case 10:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_IN) != null) ObjectHandler.getObject(ObjectHandler.DOOR_IN).interact(x -> true);
                break;
            default:
                if(NpcHandler.getNpc(NpcHandler.GUIDE) != null) NpcHandler.getNpc(NpcHandler.GUIDE).interact(x -> true);
                break;
        }
        return Random.high(322, 844);
    }
}
