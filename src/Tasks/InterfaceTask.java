package Tasks;

import Utils.InterfaceUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.script.task.Task;

public class InterfaceTask extends Task {

    public InterfaceUtil interfaceUtil = new InterfaceUtil();

    @Override
    public boolean validate() {
        return interfaceUtil.isInterfaceOpen();
    }

    @Override
    public int execute() {
        interfaceUtil.setName();
        Time.sleepUntil(() -> interfaceUtil.isInterfaceOpen(), 888);
        return Random.high(432, 758);
    }
}
