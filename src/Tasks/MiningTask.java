package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.Production;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.script.task.Task;

public class MiningTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 8 && VarpUtil.getProgress() <= 9;
    }

    @Override
    public int execute() {
        switch(VarpUtil.getSectionProgress()) {
            case 260:
                if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3078, 9507, 3083, 9501));
                } else if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) != null){ NpcHandler.getNpc(NpcHandler.MINING_GUIDE).interact(x -> true); }
                break;
            case 290:
                if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3078, 9507, 3083, 9501));
                } else if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) != null){ NpcHandler.getNpc(NpcHandler.MINING_GUIDE).interact(x -> true); }
                break;
            case 300:
                ObjectHandler.getObject(ObjectHandler.TIN_ORE).interact(x -> true);
                Time.sleepUntil(() -> Inventory.contains("Tin ore"), 2217);
                break;
            case 310:
                ObjectHandler.getObject(ObjectHandler.COPPER_ORE).interact(x -> true);
                Time.sleepUntil(() -> Inventory.contains("Copper ore"), 2217);
                break;
            case 320:
                if(!Production.isOpen()) ObjectHandler.getObject(ObjectHandler.FURNACE).interact(x -> true);
                if(Production.isOpen()) {
                    Production.initiate(0);
                    Time.sleepUntil(() -> Inventory.contains("Bronze bar"), 2384);
                }
                break;
            case 330:
                if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3078, 9507, 3083, 9501));
                } else if(NpcHandler.getNpc(NpcHandler.MINING_GUIDE) != null){ NpcHandler.getNpc(NpcHandler.MINING_GUIDE).interact(x -> true); }
                break;
            case 340:
            case 350:
                if(ObjectHandler.getObject(ObjectHandler.ANVIL) != null && !Interfaces.isOpen(312)) ObjectHandler.getObject(ObjectHandler.ANVIL).interact(x -> true);
                Time.sleepUntil(() -> Interfaces.isOpen(312), 2434);
                if(Interfaces.isOpen(312)) {
                    Interfaces.getComponent(312, 9).click();
                    Time.sleepUntil(() -> Inventory.contains("Bronze dagger"), 3722);
                }
                break;
            case 360:
                if(ObjectHandler.getObject(ObjectHandler.GATE_IN) == null) {
                    WalkUtil.walkTo(Area.rectangular(3090, 9503, 3094, 9502));
                } else if(ObjectHandler.getObject(ObjectHandler.GATE_IN) != null) { ObjectHandler.getObject(ObjectHandler.GATE_IN).interact(x -> true); }
                break;
        }

        return Random.high(33, 575);
    }
}
