package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.script.task.Task;

public class PriestTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 16 && VarpUtil.getProgress() <= 17;
    }

    @Override
    public int execute() {
        switch (VarpUtil.getSectionProgress()){
            case 550:
                if(NpcHandler.getNpc(NpcHandler.BROTHER_BRACE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3123, 3106, 3126, 3107));
                } else if(NpcHandler.getNpc(NpcHandler.BROTHER_BRACE)!= null) NpcHandler.getNpc(NpcHandler.BROTHER_BRACE).interact(x -> true);
                break;
            case 560:
                if(!Tabs.isOpen(Tab.PRAYER)) Tabs.open(Tab.PRAYER);
                Time.sleepUntil(() -> Tabs.isOpen(Tab.PRAYER), 1273);
                break;
            case 570:
                if(NpcHandler.getNpc(NpcHandler.BROTHER_BRACE) != null) NpcHandler.getNpc(NpcHandler.BROTHER_BRACE).interact(x -> true);
                break;
            case 580:
                if(!Tabs.isOpen(Tab.FRIENDS_LIST)) Tabs.open(Tab.FRIENDS_LIST);
                Time.sleepUntil(() -> Tabs.isOpen(Tab.FRIENDS_LIST), 1273);
                break;
            case 600:
                if(NpcHandler.getNpc(NpcHandler.BROTHER_BRACE) != null) NpcHandler.getNpc(NpcHandler.BROTHER_BRACE).interact(x -> true);
                break;
            case 610:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_IN) != null) {
                    ObjectHandler.getObject(ObjectHandler.DOOR_IN).interact(x -> true);
                }
                break;

        }


        return Random.high(362, 488);
    }
}
