package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.script.task.Task;

public class QuestTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 6 && VarpUtil.getProgress() <= 7;
    }

    @Override
    public int execute() {
        switch (VarpUtil.getSectionProgress()) {
            case 200:
                if(Movement.isRunEnabled()) {
                    Movement.toggleRun(false);
                    Time.sleepUntil(() -> !Movement.isRunEnabled(), 2323);
                } else {
                    Movement.toggleRun(true);
                    Time.sleepUntil(() -> Movement.isRunEnabled(), 2722);
                }
                break;
            case 210:
                if(ObjectHandler.getObject(ObjectHandler.DOOR_MIDDLE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3083, 3128, 3088, 3126));
                } else if(ObjectHandler.getObject(ObjectHandler.DOOR_MIDDLE) != null) ObjectHandler.getObject(ObjectHandler.DOOR_MIDDLE).interact(x -> true);
                break;
            case 220:
                if(NpcHandler.getNpc(NpcHandler.QUEST_GUIDE) != null) NpcHandler.getNpc(NpcHandler.QUEST_GUIDE).interact(x -> true);
                break;
            case 230:
                if(!Tab.QUEST_LIST.isOpen()) Tabs.open(Tab.QUEST_LIST);
                Time.sleepUntil(() -> Tabs.isOpen(Tab.QUEST_LIST), 3732);
                break;
            case 240:
                if(NpcHandler.getNpc(NpcHandler.QUEST_GUIDE) != null) NpcHandler.getNpc(NpcHandler.QUEST_GUIDE).interact(x -> true);
                break;
            case 250:
                if(ObjectHandler.getObject(ObjectHandler.LADDER) != null) ObjectHandler.getObject(ObjectHandler.LADDER).interact(x -> true);
                break;
        }

        return Random.high(131, 565);
    }
}
