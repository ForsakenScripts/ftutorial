package Tasks;

import Enums.NpcHandler;
import Enums.ObjectHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class SurvivalTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 2 && VarpUtil.getProgress() <= 3;
    }

    @Override
    public int execute() {
        switch(VarpUtil.getSectionProgress()){
            case 20:
                if(NpcHandler.getNpc(NpcHandler.SURVIVAL_GUIDE) == null) {
                    WalkUtil.walkTo(Area.rectangular(3099, 3098, 3106, 3094));
                } else if(NpcHandler.getNpc(NpcHandler.SURVIVAL_GUIDE) != null) NpcHandler.getNpc(NpcHandler.SURVIVAL_GUIDE).interact(x -> true);
                break;
            case 30:
                if(!Tab.INVENTORY.isOpen()) Tabs.open(Tab.INVENTORY);
                Time.sleepUntil(() -> Tab.INVENTORY.isOpen(), 3744);
                break;
            case 40:
                fishShrimp();
                break;
            case 50:
                if(!Tab.SKILLS.isOpen()) Tabs.open(Tab.SKILLS);
                Time.sleepUntil(() -> Tab.SKILLS.isOpen(), 3744);
                break;
            case 60:
                if(NpcHandler.getNpc(NpcHandler.SURVIVAL_GUIDE) != null) NpcHandler.getNpc(NpcHandler.SURVIVAL_GUIDE).interact(x -> true);
                break;
            case 70:
                chopWood();
                break;
            case 80:
            case 90:
            case 100:
            case 110:
                if(!Tab.INVENTORY.isOpen()) Tabs.open(Tab.INVENTORY);
                Time.sleepUntil(() -> Tab.INVENTORY.isOpen(), 2293);
                    if(!Inventory.contains("Raw shrimps")) fishShrimp();
                    if(!Inventory.contains("Logs")) chopWood();
                    if(Inventory.contains("Logs") && Inventory.contains("Tinderbox") && Inventory.contains("Raw shrimps")) {
                        if(ObjectHandler.getObject(ObjectHandler.FIRE) != null && ObjectHandler.getObject(ObjectHandler.FIRE).getPosition() == Players.getLocal().getPosition()){
                            WalkUtil.walkTo(Area.rectangular(3099, 3098, 3106, 3094));
                        } else {
                            Inventory.use(item -> item.getName().equals("Logs"), Inventory.getFirst("Tinderbox"));
                            Time.sleepUntil(() -> !Players.getLocal().isAnimating(), 3737);
                        }
                    }
                    if(ObjectHandler.getObject(ObjectHandler.FIRE) != null && Inventory.contains("Raw shrimps")) {
                        Inventory.use(item -> item.getName().equals("Raw shrimps"), ObjectHandler.getObject(ObjectHandler.FIRE));
                        Time.sleepUntil(() -> Inventory.getSelectedItem().equals("Raw shrimps"), 3434);
                        Inventory.use(item -> item.getName().equals("Raw shrimps"), ObjectHandler.getObject(ObjectHandler.FIRE));
                        Time.sleepUntil(() -> Inventory.contains(315), 5632);
                        }
                break;
            case 120:
                if(ObjectHandler.getObject(ObjectHandler.GATE_IN) == null) {
                    WalkUtil.walkTo(Area.rectangular(3090, 3092, 3093, 3091));
                } else {
                    ObjectHandler.getObject(ObjectHandler.GATE_IN).interact(x -> true);
                    Time.sleepUntil(() -> !Players.getLocal().isMoving(), 4403);
                }
                break;
        }
        return Random.high(132, 573);
    }

    private void fishShrimp() {
        if(NpcHandler.getNpc(NpcHandler.FISHING_SPOT) != null && !Inventory.contains("Raw shrimps")) NpcHandler.getNpc(NpcHandler.FISHING_SPOT).interact(x -> true);
        Time.sleepUntil(() -> Inventory.contains(2514), 2122);
    }

    private void chopWood() {
        if(ObjectHandler.getObject(ObjectHandler.TREE) != null) ObjectHandler.getObject(ObjectHandler.TREE).interact(x -> true);
        Time.sleepUntil(() -> Inventory.contains(1511), 3283);
    }
}
