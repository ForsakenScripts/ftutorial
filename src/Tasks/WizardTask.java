package Tasks;

import Enums.NpcHandler;
import Utils.VarpUtil;
import Utils.WalkUtil;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class WizardTask extends Task {

    @Override
    public boolean validate() {
        return !Dialog.canContinue() && VarpUtil.getProgress() >= 18 && VarpUtil.getProgress() <= 20;
    }

    @Override
    public int execute() {

        switch (VarpUtil.getSectionProgress()) {
            case 620:
                if(NpcHandler.getNpc(NpcHandler.WIZARD) == null) {
                    WalkUtil.walkTo(Area.rectangular(3140, 3087, 3143, 3085));
                } else if(NpcHandler.getNpc(NpcHandler.WIZARD) != null) NpcHandler.getNpc(NpcHandler.WIZARD).interact(x -> true);
                break;
            case 630:
                if(!Tabs.isOpen(Tab.MAGIC)) Tabs.open(Tab.MAGIC);
                Time.sleepUntil(() -> Tabs.isOpen(Tab.MAGIC), 2435);
                break;
            case 640:
                if(NpcHandler.getNpc(NpcHandler.WIZARD) != null) NpcHandler.getNpc(NpcHandler.WIZARD).interact(x -> true);
                break;
            case 650:
                if(NpcHandler.getNpc(NpcHandler.CHICKEN) != null && Magic.canCast(Spell.Modern.WIND_STRIKE)) {
                    Magic.cast(Spell.Modern.WIND_STRIKE, NpcHandler.getNpc(NpcHandler.CHICKEN));
                    Time.sleepUntil(() -> !Players.getLocal().isAnimating(), 3626);
                }
                break;
            case 670:
                if(NpcHandler.getNpc(NpcHandler.WIZARD)!= null) NpcHandler.getNpc(NpcHandler.WIZARD).interact(x -> true);
                break;
        }

        return Random.high(362, 488);
    }
}
