package Utils;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.ui.Log;


import java.util.Random;
import java.util.stream.Collectors;

public class InterfaceUtil {

    public boolean isCharacterOpen() {
        return Interfaces.isOpen(269);
    }

    public boolean canAccept(){
        return Interfaces.getComponent(269, 99).isVisible();
    }

    public void clickAccept(){
        Interfaces.getComponent(269, 99).click();
    }

    public boolean isInterfaceOpen(){
       return Interfaces.isOpen(558);
    }

    private boolean nameAvailable(){
        return Interfaces.getComponent(558, 18, 9).getTextColor() == 16750623;
    }

    private boolean clickNameAvailable(){
        return Interfaces.getComponent(558, 18).click();
    }

    private void selectName(){
        Interfaces.getComponent(558, 17).interact(x -> true);
    }

    private boolean randomNameAvailable(){
        return Interfaces.getComponent(558, 14).isVisible();
    }

    private boolean clickRandomName() {
        return Interfaces.getComponent(558, 14).click();
    }

    public void setName(){
        if(isInterfaceOpen()) {
            selectName();
            Log.info("Test");
            Time.sleep(200, 400);
            Keyboard.sendText(generateRandomString(10));
            Time.sleep(747, 1232);
            Keyboard.pressEnter();
            Time.sleepUntil(() -> randomNameAvailable() || nameAvailable(), 7223);
            }
            if(randomNameAvailable()) {
                clickRandomName();
            }
            Time.sleepUntil(() -> nameAvailable(), 3323);
            clickNameAvailable();
            Time.sleepUntil(() -> !isInterfaceOpen(), 4923);
        }


    private String generateRandomString(int maxLength) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "abcdefghijklmnopqrstuvwxyz"
                + "0123456789";
        return new Random().ints(new Random().nextInt(maxLength) + 1, 0, chars.length())
                .mapToObj(i -> "" + chars.charAt(i))
                .collect(Collectors.joining());
    }
}
