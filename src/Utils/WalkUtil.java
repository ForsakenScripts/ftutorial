package Utils;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

public class WalkUtil {

    private static boolean isWalking() {
        return Players.getLocal().isMoving();
    }


    public static void walkTo(Area area) {
        Position walkPosition = area.getCenter().randomize(1);
        if(Movement.getRunEnergy() >= Random.high(20, 35) && !Movement.isRunEnabled()) Movement.toggleRun(true);
        Movement.getDaxWalker().walkTo(walkPosition);
        Time.sleepUntil(() -> !isWalking(), Random.high(855, 1492));
    }


}
